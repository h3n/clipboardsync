﻿using System;
using System.Windows.Forms;

namespace ClipboardSync
{
    public partial class MainForm : Form
    {
        private Server server;
        public MainForm()
        {
            InitializeComponent();
            server = new Server();
            server.MessageReceived += (s, e) => { this.BeginInvoke((MethodInvoker)(() => 
            {
                try
                {
                    if (cbReceive.Checked && Clipboard.GetText() != e)
                    {
                        Clipboard.SetText(e);
                        if (cbBeep.Checked)
                        {
                            Console.Beep();
                        }
                    }
                }
                catch { }
            }));};
        }

        private void sharpClipboard_ClipboardChanged(object sender, WK.Libraries.SharpClipboardNS.SharpClipboard.ClipboardChangedEventArgs e)
        {
            if (cbSend.Checked)
            {
                Console.WriteLine(e.SourceApplication.Name + " " + e.Content.ToString());
                server.Broadcast(e.Content.ToString());
            }
        }
    }
}
