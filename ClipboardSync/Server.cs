﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClipboardSync
{
    public class Server
    {
        public delegate void MessageReceivedHandler(IPEndPoint endpoint, string message);
        public event MessageReceivedHandler MessageReceived = (a,b) => { };
        private UdpClient udpClient;
        public int Port { get; private set; }
        public Server(int port = 31337)
        {
            udpClient = new UdpClient()
            {
                MulticastLoopback = false,
                EnableBroadcast = true,
            };
            udpClient.Client.Bind(new IPEndPoint(IPAddress.Any, port));
            this.Port = port;
            var from = new IPEndPoint(0, 0);
            Task.Run(() =>
            {
                while (true)
                {

                    var recvBuffer = udpClient.Receive(ref from);
                    //if (from.Address != udpClient.)
                    if (!Dns.GetHostAddresses(Dns.GetHostName()).Contains(from.Address))
                    MessageReceived(from, Encoding.UTF8.GetString(recvBuffer));
                }
            });

        }

        public void Broadcast(string msg)
        {
            var data = Encoding.UTF8.GetBytes(msg);
            udpClient.Send(data, data.Length, "255.255.255.255", Port);
        }
        
    }
}
