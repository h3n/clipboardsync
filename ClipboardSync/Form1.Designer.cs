﻿namespace ClipboardSync
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbSend = new System.Windows.Forms.CheckBox();
            this.cbReceive = new System.Windows.Forms.CheckBox();
            this.sharpClipboard = new WK.Libraries.SharpClipboardNS.SharpClipboard(this.components);
            this.cbBeep = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cbSend
            // 
            this.cbSend.AutoSize = true;
            this.cbSend.Checked = true;
            this.cbSend.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSend.Location = new System.Drawing.Point(12, 12);
            this.cbSend.Name = "cbSend";
            this.cbSend.Size = new System.Drawing.Size(120, 17);
            this.cbSend.TabIndex = 0;
            this.cbSend.Text = "Auto send clipboard";
            this.cbSend.UseVisualStyleBackColor = true;
            // 
            // cbReceive
            // 
            this.cbReceive.AutoSize = true;
            this.cbReceive.Checked = true;
            this.cbReceive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbReceive.Location = new System.Drawing.Point(12, 36);
            this.cbReceive.Name = "cbReceive";
            this.cbReceive.Size = new System.Drawing.Size(132, 17);
            this.cbReceive.TabIndex = 1;
            this.cbReceive.Text = "Auto receive clipboard";
            this.cbReceive.UseVisualStyleBackColor = true;
            // 
            // sharpClipboard
            // 
            this.sharpClipboard.MonitorClipboard = true;
            this.sharpClipboard.ObservableFormats.All = true;
            this.sharpClipboard.ObservableFormats.Files = true;
            this.sharpClipboard.ObservableFormats.Images = true;
            this.sharpClipboard.ObservableFormats.Others = true;
            this.sharpClipboard.ObservableFormats.Texts = true;
            this.sharpClipboard.ObserveLastEntry = false;
            this.sharpClipboard.Tag = null;
            this.sharpClipboard.ClipboardChanged += new System.EventHandler<WK.Libraries.SharpClipboardNS.SharpClipboard.ClipboardChangedEventArgs>(this.sharpClipboard_ClipboardChanged);
            // 
            // cbBeep
            // 
            this.cbBeep.AutoSize = true;
            this.cbBeep.Location = new System.Drawing.Point(12, 59);
            this.cbBeep.Name = "cbBeep";
            this.cbBeep.Size = new System.Drawing.Size(156, 17);
            this.cbBeep.TabIndex = 2;
            this.cbBeep.Text = "Beep on received clipboard";
            this.cbBeep.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(172, 86);
            this.Controls.Add(this.cbBeep);
            this.Controls.Add(this.cbReceive);
            this.Controls.Add(this.cbSend);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(188, 125);
            this.MinimumSize = new System.Drawing.Size(188, 125);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Clipboard Sync";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbSend;
        private System.Windows.Forms.CheckBox cbReceive;
        private WK.Libraries.SharpClipboardNS.SharpClipboard sharpClipboard;
        private System.Windows.Forms.CheckBox cbBeep;
    }
}

